package com.sf.dubboapi.service;

public interface BaseService<T> {


    /**
     * 根据主键更新记录
     * @param t
     * @return
     */
    public Integer update(T t);

    /**
     * 根据主键查询
     * @param pk
     * @return
     */
    public T get(Long pk);

}
