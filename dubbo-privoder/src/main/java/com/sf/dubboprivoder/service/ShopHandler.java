package com.sf.dubboprivoder.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.sf.dubboapi.service.ShopApi;
import org.springframework.stereotype.Component;

@Service(interfaceClass = ShopApi.class,version = "1.0.0")
@Component
public class ShopHandler implements ShopApi {

    public String queryWithPg(String param) {
        return null;
    }

    public Integer update(String s) {
        return null;
    }

    public String get(Long pk) {
        return pk+" RESULT!";
    }
}
