package com.sf.dubboprivoder;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubboConfiguration
public class DubboPrivoderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboPrivoderApplication.class, args);
    }
}
