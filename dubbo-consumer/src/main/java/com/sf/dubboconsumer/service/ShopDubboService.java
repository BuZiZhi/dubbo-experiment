package com.sf.dubboconsumer.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.sf.dubboapi.service.ShopApi;
import org.springframework.stereotype.Component;

@Component
public class ShopDubboService {

    @Reference(version = "1.0.0")
    private ShopApi shopApi;

    public String get(Long l){
        return shopApi.get(l);
    }

}
