# dubbo-experiment

#### 项目介绍
使用dubbo-spring-starter起步依赖快速搭建dubbo分布式demo

#### 软件架构
只做测试使用 使用了Spring-Web的起步依赖以及Spring-dubbo的起步依赖


#### 安装教程

1. 直接下载解压 在IDE中引入maven项目

#### 使用说明

1. 启动dubbo-privoder中的DubboProviderApplication
2. 启动dubbo-consumer中的DubboConsumerApplication
3. 打开浏览器访问localhost:8082?param=222

#### 遇到问题

1. zkClient中依赖的日志jar包与Spring发生冲突，exclusion zkClient中的日志jar
    ` <dependency>
            <groupId>com.101tec</groupId>
            <artifactId>zkclient</artifactId>
            <version>0.10</version>
            <!-- 排除冲突依赖 -->
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-log4j12</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>log4j</groupId>
                    <artifactId>log4j</artifactId>
                </exclusion>
            </exclusions>
        </dependency>`
2. 对provider和consumer打包时候出现的api中xxx包不存在，是因为spring-boot工程打包编译时，会生成两种jar包，一种是普通的jar，另一种是可执行jar。默认情况下，这两种jar的名称相同，在不做配置的情况下，普通的jar先生成，可执行jar后生成，所以可执行jar会覆盖普通的jar。对dubbo-api中pom.xml做出以下修改：
`<build>  
    <plugins>  
        <plugin>  
            <groupId>org.springframework.boot</groupId>  
            <artifactId>spring-boot-maven-plugin</artifactId>  
            <configuration>  
                <classifier>exec</classifier>  
            </configuration>  
        </plugin>  
    </plugins>  
</build>  `

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)